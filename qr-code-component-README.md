# Frontend Mentor - QR code component solution

This is a solution to the [QR code component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
- [Author](#author)

## Overview

### Screenshot

![](images/qr-code-screenshot.png)

### Links

- Solution URL: [Gitlab](https://gitlab.com/kristinbrooks/kristinbrooks.gitlab.io/-/tree/main/public)
- Live Site URL: [QR code component](https://kristinbrooks.gitlab.io/qr-code-component.html)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties

### What I learned

I learned a bit of HTML and CSS a couple of years ago while doing some tutorials. But I don't think I've done any since then so this was my first project towards remembering what I learned but without following along with a tutorial. Learning by doing for the win!

## Author

c

