# Frontend Mentor - Order summary card solution

This is a solution to the [Order summary card challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/order-summary-component-QlPmajDUj). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
- [Author](#author)


## Overview

### The challenge

Users should be able to:

- See hover states for interactive elements

### Screenshot

![Screenshot](images/order-summary-screenshot.png)

Payment Button Hover:

![Payment Button Hover Screenshot](images/hover-button.png)

Change Plan Link Hover:

![Change Link Hover Screenshot](images/hover-change.png)

Cancel Order Link Hover:

![Cancel Order Link Hover Screenshot](images/hover-cancel.png)

### Links

- Solution URL: [Gitlab](https://gitlab.com/kristinbrooks/kristinbrooks.gitlab.io/-/tree/main/public)
- Live Site URL: [Click](https://kristinbrooks.gitlab.io/order-summary-component.html)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties

### What I learned

- Making things next to each other rather than only on top of each other
- Setting hover states

### Continued development

I still need to focus on pretty much everything. 😂

## Author

- GitLab - [@kristinbrooks](https://gitlab.com/kristinbrooks)
- GitHub - [kristinbrroks](https://github.com/kristinbrooks)
- Frontend Mentor - [@kristinbrooks](https://www.frontendmentor.io/profile/kristinbrooks)
