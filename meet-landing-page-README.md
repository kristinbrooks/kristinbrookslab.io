# Frontend Mentor - Meet landing page solution

This is a solution to the [Meet landing page challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/meet-landing-page-rbTDS6OUR). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover states for interactive elements

### Screenshot

![Mobile View](images/meet-landing-page-mobile.png)
![Tablet View](images/meet-landing-page-tablet.png)
![Desktop View](images/meet-landing-page-desktop.png)
![Button Hover](images/meet-landing-page-hover.png)

### Links

- Solution URL: [click](https://gitlab.com/kristinbrooks/kristinbrooks.gitlab.io/-/tree/main)
- Live Site URL: [click](https://kristinbrooks.gitlab.io/meet-landing-page.html)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow

### What I learned

It was my first time trying mobile first. The responsiveness was quite a bit more complicated than the 
'3 card preview component' challenge I did last but it was good to try making a whole page instead of just a component. 
I was slightly more comfortable with flexbox this time, but it's still a lot of trial and error. There a lot of little
things in this challenge that added difficulty to the previous one...making shapes with just css, adding a color layer
over a photo, etc.

### Continued development

I still need practice at almost everything really, but I think when I get more skillful at flexbox it will definitely 
help things go faster. The responsiveness stuff feels like it's going ok for my second time, but in the future I'd like 
to learn to add some transitions at my break-points so things don't just jerk to new locations. 

### Useful resources

- [css-tricks flexbox guide](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [This](https://stackoverflow.com/questions/4861224/how-to-use-css-to-surround-a-number-with-a-circle#:~:text=To%20create%20a%20circle%2C%20make,and%20height%20at%20same%20time) stackoverflow question helped me make the circles around the numbers and [this](https://stackoverflow.com/questions/32819543/draw-a-line-in-a-div) one helped me draw the lines on top.

## Author

- GitLab - [@kristinbrooks](https://gitlab.com/kristinbrooks)
- GitHub - [kristinbrroks](https://github.com/kristinbrooks)
- Frontend Mentor - [@kristinbrooks](https://www.frontendmentor.io/profile/kristinbrooks)
