# Frontend Mentor - 3-column preview card component solution

This is a solution to the [3-column preview card component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/3column-preview-card-component-pH92eAR2-). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover states for interactive elements

### Screenshot

![Desktop Screenshot](./images/desktop-3-column-preview-card-component.png)
![Button Hover Screenshot](./images/hover-3-column-preview-card-component.png)
![Mobile Screenshot](./images/mobile-3-column-preview-card-component.png)

### Links

- Solution URL: [Gitlab](https://gitlab.com/kristinbrooks/kristinbrooks.gitlab.io/-/tree/main/public)
- Live Site URL: [Click](https://kristinbrooks.gitlab.io/3-column-preview-card-component.html)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties

### What I learned

I stared learning a tiny bit about how to make things responsive. 

### Continued development

I still need to focus on pretty much everything...especially the details of what the different `position` types do and making things responsive. I couldn't get the mobile version footer to do what I wanted and I couldn't get it to be centered on the narrower screen.

## Author

- GitLab - [@kristinbrooks](https://gitlab.com/kristinbrooks)
- GitHub - [kristinbrroks](https://github.com/kristinbrooks)
- Frontend Mentor - [@kristinbrooks](https://www.frontendmentor.io/profile/kristinbrooks)
